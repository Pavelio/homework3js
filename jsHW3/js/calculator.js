"use strict";
let getNumber1 = prompt("Enter first number:");
let getNumber2 = prompt("Enter second number:");
let getOperator = prompt("Enter operator:");
let outputResult;
function isCheckNumber(number) {
    while((number == null)||(isNaN(number))) {
        number = prompt('ERROR! Enter your number:', number);
    }  
    return +number;
}
function isCalculator(number1, number2, operator) {
    switch (operator) {
        case ("+"): 
            outputResult = number1 + number2;
            break;
        case ("-"): 
            outputResult = number1 - number2;
            break;
        case ("*"): 
            outputResult = number1 * number2;
            break;
        case ("/"): 
            outputResult = number1 / number2;
            break;
        default: 
            operator = prompt("ERROR! Enter operator:");
            return isCalculator(number1, number2, operator);
    }
    return outputResult;
}
getNumber1 = isCheckNumber(getNumber1);
getNumber2 = isCheckNumber(getNumber2);
outputResult = isCalculator(getNumber1, getNumber2, getOperator);
console.log('Result = ' + outputResult);